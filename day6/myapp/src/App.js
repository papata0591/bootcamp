import React, { Component } from 'react';
import logo from './logo.svg';
import pic from './test.jpg';
import './App.css';

//----------------Random--------------

// class App extends Component {
//   render() {
//     let num = Math.floor((Math.random() * 4) + 0)
//     const loop = [...Array(3)].map((a, b) => a)
//     console.log(loop)
//     const ran = loop.map(val => 
//       (
//         <Random  num={Math.floor((Math.random() * 4) + 0)}/>
//     ))
//     return (
//       <div>{ran}</div>
//     );
//   }
// }
// const Random = (prop) =>{
//   const color = ['red', 'blue', 'green', 'purple', 'pink']
//   const fontSize = ['20px','25px','30px','35px','40px']
//     return(
//       <div className="App" style={{backgroundColor: color[prop.num],width:'300px',height:'300px'}}>
//       <p style={{fontSize:fontSize[prop.num],paddingTop:'50%'}}>RandomBox</p>
//     </div>
// )}
//------------- template-----------

class App extends Component{
  render(){
    return(
    <div>  
      <Head />
      <div style={{overflow: 'scroll'}}>
        <Body />
      </div>
      <Footer />
    </div>
    )
  }
}

class Head extends Component{
  render(){
    return(
      <div id='head'>
          <div style={{backgroundColor:'teal',color:'white',height:'50px'}}>
            <div style={{paddingTop:'10px',fontSize:'20px',paddingLeft:'10px'}}>
            <b>Recipe App</b>
            <div style={{float:'right',display: 'inline-block',paddingLeft:'10px',paddingRight :'10px' }}>asd</div>
            <div style={{float:'right',display: 'inline-block',paddingLeft:'10px' }}>asd</div>
            <div style={{float:'right',display: 'inline-block',paddingLeft:'10px' }}>asd</div>
            <div style={{float:'right',display: 'inline-block',paddingLeft:'10px' }}>asd</div>
            </div>
          </div>
      </div>
    )
  }
}
const obj = {
  name:'aaaaa',
  i:'iiii',
  c:'ccccc',
  date:'new Date()'
  // list:{
  //   q:'asd',
  //   w:'weqe',
  //   e:'qweqwe'
  // }
}
class Body extends Component{
  constructor(props) {
    super(props);
    this.state = {
      searchTxt:'',
      cardState:[{
        name:'aaaa',
        i:'aaaai',
        c:'aaaac',
        date:'aaaad'
      },
      {
        name:'aabb',
        i:'bbbbi',
        c:'bbbbc',
        date:'bbbbd'
      },
      {
        name:'aaac',
        i:'cccci',
        c:'ccccc',
        date:'ccccd'
      }]
    }
     this.cardOld = [...this.state.cardState]
  }
  delete = (value) => {
    const arr1 = this.state.cardState.filter(obj =>
      obj.name != value
    )
    console.log(arr1)
    this.setState({
      cardState: [...arr1]
   })  
  }
  search = (e)=>{
    this.setState({ searchTxt: e.target.value });
    if(e.target.value != ''){
       const arr1 = this.cardOld.filter(obj =>
        obj.name.includes(e.target.value)
      )
      this.setState({
        cardState: [...arr1]
     })
    }else{
      this.setState({
        cardState: [...this.cardOld]
     })
    }
    // console.log(this.cardOld)
    // alert('name '+this.state.searchTxt)
  }
  render(){
    const cards = this.state.cardState.map(obj =>
      <Card 
      name={obj.name}
      i={obj.i}
      c={obj.c}
      date={obj.date}
      delete={this.delete}/>)
    return(
      <div id="body">
          <div style={{paddingLeft:'43%'}}>
          <br />
             search :<input type="text" onChange={this.search} value={this.state.searchTxt}/>
          </div>
          <div >
            {cards}
          </div>
          
      </div>
    )
  }
}
const SearchBox = (prop) =>{
  return(
    <div style={{paddingLeft:'43%'}}>
      <br />
      search :<input type="text" onClick={() => prop.search()} value={prop.val}/>
    </div>
  )
}

const Card = (prop) => {
  return(
    <div style={{paddingLeft:'40px',display: 'inline-block',paddingTop:'20px'}}>
      <div id="polaroid">
      <img src={pic} style={{width:'370px',height: '200px'}} />
        <div id="container">
          <p>{prop.name}</p>
          <p>{prop.i}</p>
          {/* <ui >
            <li style={{paddingLeft:'10px'}}>{prop.list.q}</li>
            <li style={{paddingLeft:'10px'}}>{prop.list.w}</li>
            <li style={{paddingLeft:'10px'}}>{prop.list.e}</li>
          </ui> */}
          <p>{prop.c}</p>
          <div>{prop.date}</div>
          <button onClick={() => prop.delete(prop.name)}>delete</button>
        </div>
      </div>
    </div>
  )
}

class Footer extends Component{
  render(){
    return(
      <div id="footer" >
         <div style={{backgroundColor:'teal',color:'white',height:'50px',paddingBottom:'1px'}}>
            <div style={{paddingTop:'10px',paddingLeft:'45%'}}>
               This Is Footer
            </div>
         </div>
      </div>
    )
  }
}


export default App;