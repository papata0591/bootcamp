import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Home from './Home';
import About from './About';
import Me from './Me';
import Name from './Name';
import {BrowserRouter,HashRouter,Route,Link} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(<BrowserRouter> 
                    <div>
                        <Route exact path="/"  component={App} />
                        <Route path="/home" component={Home} />
                        <Route exact path="/about"  component={About} />
                        <Route exact path="/about/me" component={Me} />
                        <Route path="/about/me/:name" component={Name} />
                        <Route path="/us" component={()=>(<h3>us</h3>)} />
                    </div>
                </BrowserRouter>
    , document.getElementById('root'));
registerServiceWorker();
