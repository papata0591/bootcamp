import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      color:['red', 'blue', 'green', 'purple', 'pink','red', 'blue', 'green', 'purple', 'pink']
    }
    const arr1 = [...this.state.color]
    arr1.forEach((val, key) => {
      let randomIndex = Math.ceil(Math.random()*(key + 1));
      arr1[key] = arr1[randomIndex];
      arr1[randomIndex] = val;
    });
    this.state={
      color: [...arr1]
   }
  }
  showAlert = (value) => {
    alert('color : ' + value);
  }
  handleClick = (e) => {
    // alert('button was clicked');
    const arr1 = [...this.state.color]
    arr1.forEach((val, key) => {
      let randomIndex = Math.ceil(Math.random()*(key + 1));
      arr1[key] = arr1[randomIndex];
      arr1[randomIndex] = val;
    });
    this.setState({
      color: [...arr1]
   })  
  }
  
  render() {
    const card = this.state.color.map(val => 
      <Card color={val} showAlert={this.showAlert} />)
      console.log(card)
    return (
      <div>
          {card}
          <button onClick={this.handleClick}>swap</button>
      </div>
    );
  }
}
const Card = (prop) =>{
  return(
    <div style={{display: 'inline-block',padding:'10px'}}>
    <div onClick={() => prop.showAlert(prop.color)} className="App" style={{backgroundColor:prop.color,width:'200px',height:'200px'}}></div>
    </div>
  )
}

export default App;
