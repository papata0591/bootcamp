import { connect } from 'react-redux';
import Header from '../Header';

const mapStateToProps = state => ({
    count: state.count
});

export default connect(mapStateToProps)(Header);
