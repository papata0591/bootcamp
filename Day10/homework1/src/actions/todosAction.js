export const ADD_TODO = 'ADD';

export function add(todo) {
  return { type: ADD_TODO, text: todo };
 }