import React from 'react';

const Button = (props) => (
  <button onClick={() => props.incrementBy(10)}>increment</button>
);

export default Button;
