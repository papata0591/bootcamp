import React from 'react';

const ButtonMinus = (props) => (
  <button onClick={() => props.decrementBy(2)}>decrement</button>
);

export default ButtonMinus;
