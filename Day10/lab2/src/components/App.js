import React from 'react';
import './App.css';
import CounterContainer from '../containers/CounterContainer';
import ButtonContainer from '../containers/ButtonContainer';
import ButtonContainerMinus from '../containers/ButtonContainerMinus';

const App = () => (
  <div className="App">
    <CounterContainer />
    <ButtonContainer />
    <ButtonContainerMinus />
  </div>
);

export default App;
