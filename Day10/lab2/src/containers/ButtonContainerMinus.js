import { connect } from 'react-redux';
import { decrementBy } from '../actions/counter';
import ButtonMinus from '../components/ButtonMinus';

const mapDispatchToProps = {
  decrementBy
};

export default connect(null, mapDispatchToProps)(ButtonMinus);
