import { combineReducers } from 'redux';
import todos from './todos';
import counter from './counter';
export default combineReducers({
  todos, // มีคาเทากับ todos: todos 
  count: counter
});
