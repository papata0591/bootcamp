(function($) {
  var id = 1;
  $.fn.async = function(asyncText, promise) {
    var host = this;
    var originalHtml = $(host).html();
    $(host)
      .html(asyncText)
      .prop('disabled', true);
    promise.then(function(res) {
      $(host)
        .html(originalHtml)
        .prop('disabled', false);
    });
    return promise;
  };
  $.fn.async = function(asyncText, promise) {
    var host = this;
    var originalHtml = $(host).html();
    $(host)
      .html(asyncText)
      .prop('disabled', true);
    promise.then(function(res) {
      $(host)
        .html(originalHtml)
        .prop('disabled', false);
    });
    return promise;
  };
  $.fn.showChart = function(type, color, time) {
    id = id + 1;
    var host = this;
    $(host).append(
      //   '<div class="card" style="width: 300px;background: beige">' +
      //     '<div class="card-body">' +
      ' <div class="row">' +
        ' <div class="col-md-8">' +
        '           <canvas id="' +
        id +
        '" >' +
        '           </canvas>' +
        '       </div>' +
        ' <div class="col-md-2">' +
        '<button type="submit" onclick="del(' +
        id +
        ')"  class="del" id="d' +
        id +
        '">Delete</button>' +
        '   </div>' +
        '   </div>'
      // ' </div>' +
      // '</div>'
    );
    $('#' + id + '').realtimeChart(time, type, {
      dataset: {
        label: 'Widget ' + id,
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: color
      }
    });
    return this;
  };
})(jQuery);
