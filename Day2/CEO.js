  
class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
    }
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
          if(employee.setSalary(newSalary) == false){
            console.log(employee.firstname+"'s salary is less than before!!")
        }else{
            console.log(employee.firstname+"'s salary has been set to "+employee.getSalary())
        }

//         console.log(""+super.setSalary(newSalary))
//        console.log(""+super.setSalary(newSalary))
    }
    _golf () { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
        
    };
    _seminar(){
         this.dressCode = 'suit';
        console.log("He is going to seminar" + " Dress with :" + this.dressCode);
    }
    _fire(employee){
        this.dressCode = 'tshirt';
        console.log(employee.firstname+"has been fired!" + " Dress with :" + this.dressCode);
    };
    _hire(employee){
        this.dressCode = 'tshirt';
        console.log(employee.firstname+"has been hired back!" + " Dress with :" + this.dressCode);
    };
    gossip(employee,talk){
        console.log("Hey "+employee.firstname+", "+talk)
//        return "Hey "+employee.firstname+", "+talk
    }
}